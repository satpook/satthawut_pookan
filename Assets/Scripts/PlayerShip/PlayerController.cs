﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float playerShipSpeed = 10;
        private Vector2 movementInput = Vector2.zero;

        private float xMin;
        private float xMax;
        private float yMin;
        private float yMax;
        private float padding = 1;
        private void Start()
        {
            SetupMoveBoundaries();
        }
        private void Update()
        {
            Move();
        }
        private void SetupMoveBoundaries()
        {
            Camera gameCamera = Camera.main;
            xMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).x + padding;
            xMax = gameCamera.ViewportToWorldPoint(new Vector2(1, 0)).x - padding;
            yMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).y + padding;
            yMax = gameCamera.ViewportToWorldPoint(new Vector2(0, 1)).y - padding;
        }
        private void Move()
        {
            var inputDirection = movementInput.normalized;
            var inPutVelocity = inputDirection * playerShipSpeed;
          
            var finalVelocity = inPutVelocity  ;

            var newXPos = transform.position.x + finalVelocity.x * Time.deltaTime;
            var newYPos = transform.position.y + finalVelocity.y * Time.deltaTime;

            newXPos = Mathf.Clamp(newXPos, xMin, xMax);
            newYPos = Mathf.Clamp(newYPos, yMin, yMax);
            transform.position = new Vector2(newXPos, newYPos);
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            movementInput = context.ReadValue<Vector2>();
        }
    }
}