﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Scrip.BaseCharater
{
    public class PlayerCommun : Charater
    {
        [SerializeField] private Bullet bullet;
        [SerializeField] private EnimeyComon Enimey;
      
        [SerializeField] private AudioClip boomplayer;

       private Vector3 basePositoin;
       
       public PlayerCommun(int Hp,int Damage,GameObject gameObject) : base(Hp,Damage,gameObject)
        { 
        }
       
        public override void Fire()
        {
            bullet.Shoot();
        }
        void TakeHit()
        {
            Hp = Hp -Enimey.Damage;

            if (Hp <= 0)
            {
                Debug.Log  ("playsound");
                gameObject.transform.position = basePositoin;
                Enimey.gameObject.transform.position = Enimey.ResetGameEnimey;
                gameObject.SetActive(false);

                AudioSource.PlayClipAtPoint(boomplayer, Camera.main.transform.position, volume: 1);
                                                            
            }           
        }
        void OnTriggerEnter(Collider other)
        {
            TakeHit();
         
        }
        private void Start()
        {
            basePositoin = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
          
        }      
    }
}

