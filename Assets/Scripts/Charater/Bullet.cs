﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Scrip.BaseCharater
{
public class Bullet : MonoBehaviour
{
    public Rigidbody bullet;
        
        [SerializeField] private AudioClip shootingplayerSource;
        [SerializeField] private AudioClip shootingenemySource;

    [SerializeField] private float SpeedBullet;
        public void Shoot()
        {
            Rigidbody bulletCloning = Instantiate(bullet, transform.position, transform.rotation);
            bulletCloning.AddForce(0, SpeedBullet, 0);
            AudioSource.PlayClipAtPoint(shootingplayerSource, Camera.main.transform.position, volume: 1);
            
        }
        public void EnemyShoot()
        {

            Rigidbody bulletCloning = Instantiate(bullet, transform.position, transform.rotation);
            bulletCloning.AddForce(0, -SpeedBullet, 0);
            AudioSource.PlayClipAtPoint(shootingenemySource, Camera.main.transform.position, volume: 1);
        }
        public void EnemyShootleft()
        {

            Rigidbody bulletCloning = Instantiate(bullet, transform.position, transform.rotation);
            bulletCloning.AddForce(SpeedBullet, 0,0 );
            AudioSource.PlayClipAtPoint(shootingenemySource, Camera.main.transform.position, volume: 1);
        }
        public void EnemyShootrigth()
        {

            Rigidbody bulletCloning = Instantiate(bullet, transform.position, transform.rotation);
            bulletCloning.AddForce(-SpeedBullet, 0, 0);
            AudioSource.PlayClipAtPoint(shootingenemySource, Camera.main.transform.position, volume: 1);
        }
        public void EnemyShootUP()
        {
            Rigidbody bulletCloning = Instantiate(bullet, transform.position, transform.rotation);
            bulletCloning.AddForce(0, SpeedBullet, 0);
            AudioSource.PlayClipAtPoint(shootingenemySource, Camera.main.transform.position, volume: 1);
        }
        public void EnemyShootdownrigth()
        {
            Rigidbody bulletCloning = Instantiate(bullet, transform.position, transform.rotation);
            bulletCloning.AddForce(SpeedBullet, -SpeedBullet, 0);
            AudioSource.PlayClipAtPoint(shootingenemySource, Camera.main.transform.position, volume: 1);
        }
        public void EnemyShootrightup()
        {
            Rigidbody bulletCloning = Instantiate(bullet, transform.position, transform.rotation);
            bulletCloning.AddForce(-SpeedBullet, -SpeedBullet, 0);
            AudioSource.PlayClipAtPoint(shootingenemySource, Camera.main.transform.position, volume: 1);
        }
        public void EnemyShootdownleft()
        {
            Rigidbody bulletCloning = Instantiate(bullet, transform.position, transform.rotation);
            bulletCloning.AddForce(-SpeedBullet, SpeedBullet, 0);
            AudioSource.PlayClipAtPoint(shootingenemySource, Camera.main.transform.position, volume: 1);
        }
        public void EnemyShootleftup()
        {
            Rigidbody bulletCloning = Instantiate(bullet, transform.position, transform.rotation);
            bulletCloning.AddForce(SpeedBullet, SpeedBullet, 0);
            AudioSource.PlayClipAtPoint(shootingenemySource, Camera.main.transform.position, volume: 1);
        }

    }
}

