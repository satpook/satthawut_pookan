﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Scrip.BaseCharater
{
public class Charater : MonoBehaviour
{
       [SerializeField] internal int Hp;
       [SerializeField] internal int Damage;
       [SerializeField] private GameObject gameobject;

       public Charater(int Hp,int Damage,GameObject gameObject)
        {
            this.Hp = Hp;
            this.Damage = Damage;
            gameobject = gameObject;
        }
        public virtual void Fire()
        {

        }
}
}

