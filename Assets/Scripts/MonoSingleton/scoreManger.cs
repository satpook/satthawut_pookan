﻿using GameManage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scoreManger : MonoSingleton<scoreManger>
{
    private int scoreInput;
   
    public void Upscore(int scored)
    {
        scoreInput += scored;  
    }
    public void Resetscore()
    {
        scoreInput = 0;
    }
    public int Showscore()
    {
        return scoreInput;  
    }
}
