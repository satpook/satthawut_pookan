﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using  Scrip.BaseCharater;
using GameManage;

namespace Scrip.UI
{
public class GameManger : MonoSingleton<GameManger>
    {
        [SerializeField] private PlayerCommun Player;
        [SerializeField] private EnimeyComon Enemy;
        [SerializeField] private GameObject MainStartUI;       
        [SerializeField] private GameObject ExitUI;
       
       
       
            
        private int HPEnimey;
        private int HPPlayer;
        
        
        
        public Text textScoreEndGame;
        
        public void Start()
        {
            HPEnimey = Enemy.Hp;
            HPPlayer = Player.Hp;
            Player.gameObject.SetActive(false);
            Enemy.gameObject.SetActive(false);
           
            ExitUI.gameObject.SetActive(false);
           
                        
        }
        private void Update()
        {
            
            if (Enemy.Hp <=0)
            {
                Exitgame();             
            }
            if (Player.Hp <= 0)
            {
                Exitgame();
            }

        }
       public void ClickGamestart()
        {
            MainStartUI.gameObject.SetActive(false);
            Player.gameObject.SetActive(true);
            Enemy.gameObject.SetActive(true);
            ExitUI.gameObject.SetActive(false);
        }
       public void Exitgame()
        {
              
            ExitUI.gameObject.SetActive(true);
            textScoreEndGame.text = $"Score Player : {scoreManger.Instance.Showscore()}";
            
            Player.gameObject.SetActive(false);
            Enemy.gameObject.SetActive(false);
        }
        public void ClilQuit()
        {
            
            Application.Quit();
        }
        public void Clikrestart()
        {
            Enemy.Hp = HPEnimey;
            Player.Hp = HPPlayer;
            scoreManger.Instance.Resetscore();
            ClickGamestart();
        }
        public void Exit()
        {
            scoreManger.Instance.Resetscore();
            SceneManager.LoadScene("Game");
        }
    }
}


